import { useAppValues } from "contexts/AppContext";
import { useState } from "react";
import { Modal, Input, message } from "antd";
import { nanoid } from "nanoid";

function FriendsList() {
  const { friends, removeFriend, addFriend } = useAppValues();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [name, setName] = useState("");

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    if (!name.trim()) {
      message.error("Please enter a name");
      return;
    }
    handleAddMember();
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleAddMember = () => {
    addFriend({ name, id: nanoid(), balance: 0 });
    setName("");
  };

  return (
    <div className="lg:w-40 w-28 h-full">
      <div className="uppercase font-bold text-gray-500 h-5">Friends</div>
      <div
        className="h-8 uppercase text-slate-700 font-semibold py-1 bg-gray-200 dark:text-slate-900 cursor-pointer px-1 rounded-md my-1 flex items-center justify-between"
        onClick={showModal}
      >
        Add friend
        <span className="pr-2">+</span>
      </div>
      <div className="h-[calc(100%-3.25rem)] overflow-auto">
        {friends.map((friend) => (
          <div
            key={friend.id}
            className="group py-1 hover:bg-gray-200 dark:hover:text-slate-900 cursor-pointer px-1 rounded-md my-1 flex items-center justify-between"
          >
            {friend.name}
            <span
              className="hidden group-hover:inline-block pr-2 text-red-300"
              onClick={() => removeFriend(friend.id)}
            >
              x
            </span>
          </div>
        ))}
      </div>
      <Modal
        title="Basic Modal"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Input
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
          onKeyDown={(event) => {
            if (event.key === "Enter") {
              handleOk();
            }
          }}
          placeholder="Enter your friend's name"
          required
        />
      </Modal>
    </div>
  );
}

export default FriendsList;
