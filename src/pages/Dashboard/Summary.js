import { Avatar } from "antd";
import { getCurrencyValue } from "utils";

function Summary(props) {
  const { friendsToPay, friendsToReceive } = props;

  return (
    <div className="mt-8 flex h-[calc(100%-9.5rem)]">
      <SummarySection title="You owe">
        <UserList
          data={friendsToPay}
          cashClassName="text-orange-600"
          prefix="you owe "
        />
      </SummarySection>
      <SummarySection title="You are owed">
        <UserList
          data={friendsToReceive}
          cashClassName="text-green-600"
          prefix="owes you "
        />
      </SummarySection>
    </div>
  );
}

const SummarySection = ({ title, children }) => {
  return (
    <div className="flex-1">
      <div className="uppercase font-bold text-gray-500 pl-2 h-6">{title}</div>
      {children}
    </div>
  );
};

const UserList = (props) => {
  const { data, cashClassName = "", prefix = "" } = props;
  return (
    <div className="h-[calc(100%-1.5rem)] overflow-auto">
      {data.map((item) => (
        <div className="flex gap-4 mt-2" key={item.id}>
          <Avatar size="large">{item.name[0]}</Avatar>
          <div className="flex-1">
            <div className="font-medium">{item.name}</div>
            <div className={cashClassName}>{`${prefix}${getCurrencyValue(
              Math.abs(item.balance)
            )}`}</div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Summary;
