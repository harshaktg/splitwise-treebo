import { useAppValues } from "contexts/AppContext";
import Header from "./Header";
import Summary from "./Summary";
import ExpenseDrawer from "components/ExpenseDrawer";

const getFriendsToPay = (friends) => {
  if (!friends) return [];
  return friends.filter((frnd) => frnd.balance < 0);
};

const getFriendsToReceive = (friends) => {
  if (!friends) return [];
  return friends.filter((frnd) => frnd.balance > 0);
};

export const getSummarizedValues = (friends) => {
  const toPay = getFriendsToPay(friends).reduce(
    (prev, curr) => (prev += curr.balance),
    0
  );
  const toReceive = getFriendsToReceive(friends).reduce(
    (prev, curr) => (prev += curr.balance),
    0
  );
  const total = toPay + toReceive;
  return { toPay, toReceive, total };
};

function Dashboard() {
  const { friends } = useAppValues();
  return (
    <>
      <div className="h-8 flex justify-between mb-8">
        <span className="text-xl font-bold">Dashboard</span>
        <ExpenseDrawer />
      </div>
      <Header />
      <Summary
        friendsToPay={getFriendsToPay(friends)}
        friendsToReceive={getFriendsToReceive(friends)}
      />
    </>
  );
}

export default Dashboard;
