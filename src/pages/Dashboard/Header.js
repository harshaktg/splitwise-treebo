import { useAppValues } from "contexts/AppContext";
import { useEffect, useState, useCallback } from "react";
import { getCurrencyValue } from "utils";
import { getSummarizedValues } from "./index";

const SECTIONS = [
  {
    id: "total",
    name: "total balance",
    absolute: false,
  },
  {
    id: "toPay",
    name: "you owe",
    absolute: true,
  },
  {
    id: "toReceive",
    name: "you are owed",
    absolute: true,
  },
];

function DashboardHeader() {
  const { friends } = useAppValues();
  const [sections, setSections] = useState(SECTIONS);

  useEffect(() => {
    if (friends?.length) {
      const getValue = (id) => {
        if (!id) return 0;
        const { toPay, toReceive, total } = getSummarizedValues(friends);
        switch (id) {
          case "toPay":
            return toPay;
          case "toReceive":
            return toReceive;
          case "total":
            return total;
          default:
            return 0;
        }
      };

      const updatedSections = SECTIONS.map((section) => ({
        ...section,
        value: getValue(section.id),
      }));
      setSections(updatedSections);
    } else {
      const updatedSections = SECTIONS.map((section) => ({
        ...section,
        value: 0,
      }));
      setSections(updatedSections);
    }
  }, [friends]);

  const getTextClass = useCallback((value) => {
    if (!value) return "";
    return value < 0 ? "text-orange-600" : "text-green-600";
  }, []);

  return (
    <div className="flex items-center divide-x h-14">
      {sections.map(({ id, name, value, absolute }) => (
        <div key={id} className="flex-1 text-center">
          <div>{name}</div>
          <div className={`text-2xl ${getTextClass(value)}`}>
            {absolute
              ? getCurrencyValue(Math.abs(value))
              : getCurrencyValue(value)}
          </div>
        </div>
      ))}
    </div>
  );
}

export default DashboardHeader;
