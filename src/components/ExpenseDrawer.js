import { Button, Drawer, Select, Input, Form, Checkbox } from "antd";
import { useAppValues } from "contexts/AppContext";
import { useCallback, useState } from "react";

function ExpenseDrawer() {
  const [form] = Form.useForm();
  const { friends, addExpense } = useAppValues();
  const [visible, setVisible] = useState(false);
  const [selectedFriends, setSelectedFriends] = useState([]);
  const [payers, setPayers] = useState([]);
  const [description, setDescription] = useState("");
  const [amount, setAmount] = useState("");
  const [splitValues, setSplitValues] = useState([
    { id: "0", name: "You", checked: true, amount: 0 },
  ]);

  const handleDescriptionChange = useCallback((e) => {
    setDescription(e.target.value);
  }, []);

  const handleAmountChange = useCallback((e) => {
    setAmount(e.target.value);
  }, []);

  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const clearForm = () => {
    setSelectedFriends([]);
    setPayers([]);
    setDescription("");
    setAmount("");
    setSplitValues([{ id: "0", name: "You", checked: true, amount: 0 }]);
  };

  const onFinish = (values) => {
    const filteredSelectedFriends = splitValues.filter(
      (friend) => friend.checked
    );
    const allInvolvedFriends = [...values.selectFriends, "0"];
    const includedFriends = allInvolvedFriends.filter((id) =>
      filteredSelectedFriends.find((friend) => friend.id === id)
    );
    console.log("Success:", { ...values, includedFriends });
    addExpense(values.amount, values.payer, includedFriends);
    form.resetFields();
    clearForm();
    onClose();
  };

  const onFinishFailed = (errorInfo) => {
    console.error("Failed:", errorInfo);
  };

  const handleCheckToggle = (friendId, val) => {
    const updatedSplitValues = [...splitValues].map((frnd) => {
      if (frnd.id === friendId) {
        return { ...frnd, checked: val };
      }
      return frnd;
    });
    setSplitValues(updatedSplitValues);
  };

  return (
    <>
      <Button
        type="primary"
        shape="round"
        className="bg-blue-400"
        onClick={showDrawer}
      >
        Add expense
      </Button>
      <Drawer
        title="Add expense"
        placement="right"
        onClose={onClose}
        visible={visible}
        className="dark:bg-slate-800"
      >
        <Form
          form={form}
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="With you and:"
            name="selectFriends"
            rules={[{ required: true, message: "Please select your friends!" }]}
          >
            <Select
              mode="multiple"
              placeholder="Select friends"
              key={"friends-selection"}
              value={selectedFriends}
              onChange={(val) => {
                setSelectedFriends(val);
                setSplitValues([
                  { id: "0", name: "You", checked: true, amount: 0 },
                  ...val.map((id) => ({
                    ...friends.find((friend) => friend.id === id),
                    checked: true,
                    amount: 0,
                  })),
                ]);
              }}
              className="w-full"
            >
              {friends.map((friend) => (
                <Select.Option key={friend.id}>{friend.name}</Select.Option>
              ))}
            </Select>
          </Form.Item>

          {selectedFriends?.length > 0 && (
            <div className="mt-8">
              <Form.Item
                label="Description"
                name="description"
                rules={[
                  { required: true, message: "Please enter the description!" },
                ]}
              >
                <Input
                  placeholder="Enter description"
                  value={description}
                  onChange={handleDescriptionChange}
                />
              </Form.Item>
              <Form.Item
                label="Amount"
                name="amount"
                rules={[
                  { required: true, message: "Please enter the amount!" },
                ]}
              >
                <Input
                  placeholder="0.00"
                  value={amount}
                  onChange={handleAmountChange}
                  type="number"
                />
              </Form.Item>
              <Form.Item
                label="Payer"
                name="payer"
                rules={[{ required: true, message: "Please enter the payer!" }]}
              >
                <Select
                  //   mode="multiple"
                  placeholder="Select Payer"
                  value={payers}
                  onChange={setPayers}
                  className="w-full"
                >
                  {splitValues.map((friend) => (
                    <Select.Option key={friend.id}>{friend.name}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item>
                {splitValues.map((friend) => (
                  <div key={friend.id} className="flex items-center gap-4 mt-2">
                    <Checkbox
                      onChange={(e) =>
                        handleCheckToggle(friend.id, e.target.checked)
                      }
                      checked={friend.checked}
                    />
                    <span>{friend.name}</span>
                    <Input
                      type="number"
                      onChange={console.log}
                      value={
                        friend.checked && amount
                          ? (
                              amount /
                              splitValues.filter((friend) => friend.checked)
                                .length
                            ).toFixed(2) || 0
                          : 0
                      }
                      disabled
                    />
                  </div>
                ))}
              </Form.Item>
              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button
                  type="primary"
                  htmlType="submit"
                  className="bg-blue-400"
                >
                  Submit
                </Button>
              </Form.Item>
            </div>
          )}
        </Form>
      </Drawer>
    </>
  );
}

export default ExpenseDrawer;
