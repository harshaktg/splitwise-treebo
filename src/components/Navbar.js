import { Switch } from "antd";

function Navbar(props) {
  const { theme, setTheme } = props;
  return (
    <nav className="flex items-center justify-between h-16 lg:px-48 px-4 sm:px-24 bg-green-500 dark:bg-slate-800 shadow-md">
      <span className="font-semibold text-lg text-white">Treebo Splitwise</span>
      <Switch
        checked={theme === "dark"}
        onChange={(val) => setTheme(val ? "dark" : "light")}
        className={theme === "dark" ? "bg-blue-400" : "bg-gray-400"}
      />
    </nav>
  );
}

export default Navbar;
