import Navbar from "components/Navbar";
import Dashboard from "pages/Dashboard";
import useLocalStorage from "hooks/useLocalStorage";
import { AppProvider } from "contexts/AppContext";
import FriendsList from "pages/Friends";

function App() {
  const [theme, setTheme] = useLocalStorage("theme", "light");

  return (
    <AppProvider>
      <div className={`h-full ${theme}`}>
        <div className="h-full bg-white dark:bg-slate-900 text-slate-900 dark:text-white">
          <Navbar theme={theme} setTheme={setTheme} />
          <div className="lg:mx-48 sm:mx-24 mx-4 py-4 h-[calc(100%-4rem)] flex gap-8">
            <div className="hidden md:block">
              <FriendsList />
            </div>
            <div className="flex-1">
              <Dashboard />
            </div>
          </div>
        </div>
      </div>
    </AppProvider>
  );
}

export default App;
