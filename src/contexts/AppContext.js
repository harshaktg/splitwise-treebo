import { createContext, useContext, useState } from "react";
import { FRIENDS } from "constants";

const AppContext = createContext();

const AppProvider = ({ children }) => {
  const [friends, setFriends] = useState(FRIENDS);
  // const [expenses, setExpenses] = useState([]);
  const addFriend = (friendObj) => {
    const updatedFriends = [...friends, friendObj];
    setFriends(updatedFriends);
  };
  const removeFriend = (id) => {
    const updatedFriends = friends.filter((frnd) => frnd.id !== id);
    setFriends(updatedFriends);
  };

  const addExpense = (amount, payerId, payeesIds) => {
    const originalAmount = +amount;
    const splitAmount = (originalAmount / payeesIds.length).toFixed(2);
    const updatedFriends = [...friends].map((friend) => {
      if (payeesIds.includes(friend.id)) {
        if (friend.id === payerId && payeesIds.includes("0")) {
          friend.balance = friend.balance - +splitAmount;
        } else if (payerId === "0") {
          friend.balance = friend.balance + +splitAmount;
        }
      } else if (payeesIds.includes("0")) {
        if (friend.id === payerId) {
          friend.balance = friend.balance - +splitAmount;
        }
      }
      return friend;
    });
    setFriends(updatedFriends);
  };
  return (
    <AppContext.Provider
      value={{ friends, addFriend, removeFriend, addExpense }}
    >
      {children}
    </AppContext.Provider>
  );
};

const useAppValues = () => {
  return useContext(AppContext);
};

export { AppProvider, useAppValues };
